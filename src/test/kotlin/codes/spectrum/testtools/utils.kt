package codes.spectrum.testtools

import kotlin.reflect.KCallable

val USE_TEST_PRINT = (System.getenv("TEST_PRINT") ?: "false").toBoolean()
fun testprint(vararg values: Any?) {
    if (!USE_TEST_PRINT) return
    println(
        values.map { if (it is KCallable<*>) it.call() else it }.joinToString(", ")
    )
}