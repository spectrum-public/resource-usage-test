package codes.spectrum.testtools.memory

import codes.spectrum.testtools.testprint
import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec

internal class MemoryConsumptionBenchmarkTest : StringSpec({

    "detects normal usage"{
        val result = MemoryConsumptionBenchmark(
            { 1 },
            { 10_000_000 },
            { },
            { _, t -> arrayOfNulls<String>(1000) }
        ).execute().also { testprint(it) }
        result.ok shouldBe true
    }

    "detects leak"{
        val outer_list = mutableListOf<String?>()
        val result = MemoryConsumptionBenchmark(
            { 1 },
            { 10_000_000 },
            { },
            { _, t -> repeat(10000) { outer_list.add(t.toString()) } }
        ).execute().also { testprint(it) }
        result.hasLeak shouldBe true
        result.hasScale shouldBe false
    }

    "detects scales"{
        val result = MemoryConsumptionBenchmark(
            { arrayOf(1, 2, 3) },
            { arrayOf(1, 2, 3, 4, 5, 1, 2, 3, 41, 2, 3, 4, 5, 1, 2, 3, 41, 2, 3, 4, 5, 1, 2, 3, 41, 2, 3, 4, 5, 1, 2, 3, 41, 2, 3, 4, 5, 1, 2, 3, 41, 2, 3, 4, 5, 1, 2, 3, 41, 2, 3, 4, 5, 1, 2, 3, 41, 2, 3, 4, 5, 1, 2, 3, 41, 2, 3, 4, 5, 1, 2, 3, 41, 2, 3, 4, 5, 1, 2, 3, 41, 2, 3, 4, 5, 1, 2, 3, 41, 2, 3, 4, 5, 1, 2, 3, 41, 2, 3, 4, 5, 1, 2, 3, 41, 2, 3, 4, 5, 1, 2, 3, 41, 2, 3, 4, 5, 1, 2, 3, 4) },
            { mutableListOf<String>() },
            { r, t -> repeat(2) { r.addAll(t.map { toString() }) } }
        ).execute().also { testprint(it) }
        result.hasScale shouldBe true
        result.hasLeak shouldBe false
    }


    "detects normal usage - with extension from MemoryConsumptionBenchmark"{
        MemoryConsumptionBenchmark(
            { 1 },
            { 10_000_000 },
            { },
            { _, t -> arrayOfNulls<String>(1000) }
        ).haveNormalMemory()
    }

    "detects normal usage - with extension from values"{
        { _: Int -> arrayOfNulls<String>(1000) }.haveNormalMemory(1, 10_000_000)
    }

    "detects normal usage - with extension no values"{
        { arrayOfNulls<String>(1000) }.haveNormalMemory()
    }

    if (System.getenv().getOrDefault("TEST_EXPERIMENTS", "false").toBoolean()) {
        for (s in 0..10) {
            val leak = (MemoryConsumptionBenchmarkResult.LEAK_TRESHOLD - (s * MemoryConsumptionBenchmarkResult.LEAK_TRESHOLD / 100.0)).toInt()
            val scale = (MemoryConsumptionBenchmarkResult.SCALE_THRESHOLD + (s * MemoryConsumptionBenchmarkResult.SCALE_THRESHOLD / 100.0)).toDouble()
            "detects normal usage ${s} leak ${leak} scale ${scale}"{
                val result = MemoryConsumptionBenchmark(
                    { 1 },
                    { 10_000_000 },
                    { },
                    { _, t -> arrayOfNulls<String>(1000) },
                    leak_threshold = leak,
                    scale_threshold = scale
                ).execute().also { testprint(it) }
                result.ok shouldBe true
            }
            "detects leak  ${s}  leak ${leak} scale ${scale}"{
                val outer_list = mutableListOf<String?>()
                val result = MemoryConsumptionBenchmark(
                    { 1 },
                    { 10_000_000 },
                    { },
                    { _, t -> repeat(10000) { outer_list.add(t.toString()) } },
                    leak_threshold = leak,
                    scale_threshold = scale
                ).execute().also { testprint(it) }
                result.hasLeak shouldBe true
                result.hasScale shouldBe false
            }
            "detects scales  ${s}  leak ${leak} scale ${scale}"{
                val result = MemoryConsumptionBenchmark(
                    { arrayOf(1, 2, 3) },
                    { arrayOf(1, 2, 3, 4, 5, 1, 2, 3, 41, 2, 3, 4, 5, 1, 2, 3, 41, 2, 3, 4, 5, 1, 2, 3, 41, 2, 3, 4, 5, 1, 2, 3, 41, 2, 3, 4, 5, 1, 2, 3, 41, 2, 3, 4, 5, 1, 2, 3, 41, 2, 3, 4, 5, 1, 2, 3, 41, 2, 3, 4, 5, 1, 2, 3, 41, 2, 3, 4, 5, 1, 2, 3, 41, 2, 3, 4, 5, 1, 2, 3, 41, 2, 3, 4, 5, 1, 2, 3, 41, 2, 3, 4, 5, 1, 2, 3, 41, 2, 3, 4, 5, 1, 2, 3, 41, 2, 3, 4, 5, 1, 2, 3, 41, 2, 3, 4, 5, 1, 2, 3, 4) },
                    { mutableListOf<String>() },
                    { r, t -> repeat(2) { r.addAll(t.map { toString() }) } },
                    leak_threshold = leak,
                    scale_threshold = scale
                ).execute().also { testprint(it) }
                result.hasScale shouldBe true
                result.hasLeak shouldBe false
            }
        }
    }


})