package codes.spectrum.testtools.memory

data class MemoryConsumptionBenchmarkResult(
    val before: Long = 0,
    val after: Long = 0,
    val etalon: Long = 0,
    val control: Long = 0,
    val leak_threshold: Int = LEAK_TRESHOLD,
    val scale_threshold: Double = SCALE_THRESHOLD,
    val hasLeak: Boolean = (after - before) > leak_threshold,
    val hasScale: Boolean = (etalon.toDouble() / control.toDouble()) < scale_threshold
) {

    val ok get() = !hasLeak && !hasScale

    companion object {
        val LEAK_TRESHOLD = 256
        val SCALE_THRESHOLD = 0.8
    }
}

