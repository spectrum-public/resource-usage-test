package codes.spectrum.testtools.memory

import io.kotlintest.Matcher
import io.kotlintest.Result
import io.kotlintest.should

private fun benchmarkHaveNormalMemory() = object : Matcher<MemoryConsumptionBenchmark<*, *>> {
    override fun test(value: MemoryConsumptionBenchmark<*, *>): Result {
        val result = value.execute()
        return Result(value.execute().ok, "Have some problem with memory: ${result}", "Have not problem with memory: ${result}")
    }
}

fun MemoryConsumptionBenchmark<*, *>.haveNormalMemory() = this should benchmarkHaveNormalMemory()

private fun execHaveNormalMemory() = object : Matcher<() -> Any?> {
    override fun test(value: () -> Any?) = benchmarkHaveNormalMemory().test(MemoryConsumptionBenchmark(
        {},
        {},
        {},
        { _, _ -> value() }
    ))
}

private fun <T> execHaveNormalMemory(etalon: T, control: T) = object : Matcher<(T) -> Any?> {
    override fun test(value: (T) -> Any?): Result =

        benchmarkHaveNormalMemory().test(MemoryConsumptionBenchmark(
            { etalon },
            { control },
            {},
            { _, t -> value(t) }
        ))


}

fun (() -> Any?).haveNormalMemory() = this should execHaveNormalMemory()
fun <T> ((T) -> Any?).haveNormalMemory(etalon: T, control: T = etalon) = this should execHaveNormalMemory(etalon, control)