package codes.spectrum.testtools.memory


class MemoryConsumptionBenchmark<TArgs, TTarget>(
    val etalonprobe: () -> TArgs = { null as TArgs },
    val controlprobe: () -> TArgs = etalonprobe,
    val createtarget: () -> TTarget = { null as TTarget },
    val executor: (TTarget, TArgs) -> Unit = { _, _ -> },
    val samples: Int = DEFAULT_SAMPLES,
    val leak_threshold: Int = MemoryConsumptionBenchmarkResult.LEAK_TRESHOLD,
    val scale_threshold: Double = MemoryConsumptionBenchmarkResult.SCALE_THRESHOLD
) {
    companion object {
        const val DEFAULT_SAMPLES = 10
        val runtime = Runtime.getRuntime()
    }

    private fun usage() = runtime.totalMemory() - runtime.freeMemory()
    private fun play(probe: () -> TArgs, _samples: Int = samples): Long {
        val probes = mutableListOf<Long>()
        repeat(_samples) {
            val arguments = probe()
            val target = createtarget()
            runtime.gc()
            val before = usage()
            executor(target, arguments)
            val after = usage()
            probes.add(after - before)
        }
        return probes.average().toLong()
    }

    fun execute(): MemoryConsumptionBenchmarkResult {
        //warm_up
        play(etalonprobe)
        play(controlprobe)
        runtime.gc()
        val before = usage()
        val etalon = play(etalonprobe)
        val control = play(controlprobe)
        runtime.gc()
        val after = usage()
        return MemoryConsumptionBenchmarkResult(before, after, etalon, control, leak_threshold, scale_threshold)
    }
}

