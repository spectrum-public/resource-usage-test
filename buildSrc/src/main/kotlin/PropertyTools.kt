import org.gradle.api.Project
import org.gradle.kotlin.dsl.extra

inline fun <reified T> Project.ensureProperty(name: String, defaultValue: T): T {
    if (null == this.findProperty(name)) {
        this.extra.set(name, defaultValue)
    }
    if (T::class == String::class) {
        return this.ensureProperty(name) as T
    }
    val value = this.findProperty(name)!!
    if(value is T)return value
    if(T::class==Boolean::class){
        return value.toString().toBoolean() as T
    }
    if(T::class.java==Int::class.java){
        return value.toString().toDouble().toInt() as T
    }
    return value as T
}

inline fun <reified T> Project.ensureProperty(name: String): T {
    if (null == this.findProperty(name)) throw Exception("Cannot find property ${name}")
    if (T::class == String::class) {
        return this.ensureProperty(name) as T
    }
    return this.findProperty(name) as T
}

fun Project.ensureProperty(name: String): String {
    if (null == this.findProperty(name)) throw Exception("Cannot find property ${name}")
    return this.findProperty(name)!!.toString()
}
