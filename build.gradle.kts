plugins {
    id("io.gitlab.arturbosch.detekt") version "1.0.0-RC14"
    `maven-publish`
    id("com.jfrog.bintray") version "1.8.4"
    id("com.jfrog.artifactory") version "4.9.0"
}
setupRepositories()
setupKotlin()
setupTests()
dependencies{
    //coroutines()
    //logback()
    //gson()
}

dependencies {
    add("compile", "io.kotlintest:kotlintest-core:3.3.2")
}

version = "0.5.0"
group = "codes.spectrum.public"
val self = this
publishing{
    publications {
        create<MavenPublication>("maven") {
            groupId = self.group.toString()
            artifactId = self.name
            version = self.version.toString()
            from(components["java"])
        }
    }
}

if(tasks.findByName("deploy")==null){
    tasks.register("deploy") {
        group = "publishing"
    }
}

bintray {
    user = System.getenv("BINTRAY_USER")
    key = System.getenv("BINTRAY_KEY")

    setPublications("maven")
    pkg.apply {
        repo = "spectrum-public"
        name = "resource-usage-test"
        userOrg = "spectrum-project"
        vcsUrl = "https://gitlab.com/spectrum-public/templates/resource-usage-test.git"
        override = true
        setLicenses("Apache-2.0")
        publish = true
        setPublications("maven")
    }
}

tasks{

    val check by existing
    val detekt by existing
    val deploy by existing
    val bintrayUpload by existing
    publish {
        dependsOn(bintrayUpload)
    }

    if(ensureProperty("with-detekt",false)) {
        check {
            dependsOn(detekt)
        }
    }

    if(ensureProperty("publish",false)) {
        deploy {
            dependsOn(publish)
        }
    }
}